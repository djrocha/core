/*
 *-------------------------------------------------------------------
 * Nombre		: LIB_BUS_FUNCIONES.esql
 * Proposito	: Funciones y procedimientos de ayuda
 * Fecha		: 2017-08-04
 * Autores		: Javier Quintana - Synopsis
 * Version		: 4.0
 *-------------------------------------------------------------------
 */
BROKER SCHEMA ib.bus.core.common


/*
* Valida el tipo de respuesta de servicio canonico del BUS
*/
CREATE PROCEDURE obtenerStatusRespSrv(IN refInputBody REFERENCE, IN refEnvResp REFERENCE)
BEGIN
	SET refEnvResp.StatusRes.status = refInputBody.nsSoap:Envelope.nsSoap:Header.nsHeaderBus:HeaderResponse.status;
END;

/*
 * obtiene la cola de servicio para las invaciones dentro de un compuesto
*/
CREATE FUNCTION getColaSrvCanonico(IN refEnv REFERENCE,IN chrTipo CHARACTER ) 
	RETURNS CHARACTER 
BEGIN 
	
	IF (getParmSecSrv(refEnv,'VERSION') = 'v1.0') THEN
		RETURN  'IB.SRV.'|| getParmSecSrv(refEnv,'CODIGO') || '.' || chrTipo;
	ELSE
		
		RETURN  'IB.SRV.'|| getParmSecSrv(refEnv,'CODIGO')|| '.' ||UPPER(getParmSecSrv(refEnv,'VERSION')) || '.' || chrTipo;
		
	END IF;
END;


/*
 * obtiene el parametro del servicio de una secuencia
 * previamente se debe haber seteado la variable Environment.Srv.Secuencia si es que se quiere sacar 
 * un parametro de alguna de las secciones <SECUENCIA> del archivo de configuracion del servicio.
 * Si no se setea entonces se saca un parametro general del archivo de configuracion. 
*/
CREATE FUNCTION getParmSecSrv(IN refEnv REFERENCE, IN chrParm CHARACTER) 
	RETURNS CHARACTER
BEGIN
	IF (refEnv.Srv.Secuencia IS NULL) THEN
		RETURN	refEnv.Parametros.{chrParm};
	ELSE
		RETURN  refEnv.Parametros.{'SERVICIO_' || refEnv.Srv.Secuencia}.{chrParm};
	END IF;		
END;



/*
 * genera messaid Unico para transacciones
*/
CREATE FUNCTION getMsgId() 
	RETURNS CHARACTER
BEGIN
	RETURN LEFT(BrokerName || REPLICATE('0',12) ,12) || REPLACE(UUIDASCHAR,'-','0');
END;

/*
 * genera messaid Unico para transacciones en Blob
*/
CREATE FUNCTION getMsgIdBlob() 
	RETURNS BLOB
BEGIN
	RETURN CAST(LEFT(BrokerName || '0000' ,8) AS BLOB CCSID 819) || UUIDASBLOB;
END;

/*
 * Funciones que manejan la fecha y hora de la auditoria en BD
*/
CREATE FUNCTION obtenerFechaHoraAuditoria()
RETURNS CHARACTER
BEGIN
	RETURN CAST(CURRENT_TIMESTAMP AS CHAR FORMAT C_FORMATO_FECHAHORA_BD);				
END;

/*
 * Timestamp para CWF
*/
CREATE FUNCTION obtenerFechaHoraCWF()
RETURNS CHARACTER
BEGIN
	RETURN CAST(CURRENT_TIMESTAMP AS CHAR FORMAT C_FORMATO_FECHAHORA_CWF);				
END;

/*
 * Obtiene la hora en cadena
*/
CREATE FUNCTION obtenerHoraAuditoria()
RETURNS CHARACTER
BEGIN
	RETURN CAST(CURRENT_TIME AS CHAR FORMAT C_FORMATO_HORA_BD);				
END;

/* 
 * Devuelve solo una cadena con el error
*/ 
CREATE FUNCTION obtenerMensajeError(IN refExceptionList REFERENCE) RETURNS CHARACTER
BEGIN
	DECLARE chrCodigoError CHARACTER ;
	DECLARE chrDescripcionError CHARACTER '';
	
	CALL obtenerDetalleError(refExceptionList,chrCodigoError,chrDescripcionError);
	RETURN chrCodigoError ||'-'||chrDescripcionError ;
END;
	
/*
 * Obtiene el tipo de codigo de error para poder identificar de donde se origino el problema
*/ 
CREATE FUNCTION obtenerTipoError(IN intCodError INTEGER) RETURNS INTEGER
    BEGIN
	
 	RETURN 
    CASE 
    WHEN (intCodError>0 AND intCodError<1000) THEN C_TIP_ERROR_0  --Errores lanzados por la logica del BUS
    WHEN (intCodError>=1000 AND intCodError<=1999) THEN C_TIP_ERROR_1 --Configuracion   
    WHEN (intCodError>=2000 AND intCodError<=2999) THEN C_TIP_ERROR_2 --Intermediario
    WHEN (intCodError>=3000 AND intCodError<=3999) THEN C_TIP_ERROR_3 --Nodos
    WHEN (intCodError>=4000 AND intCodError<=4999) THEN C_TIP_ERROR_4 --Nodos Incorporados
    ELSE C_TIP_ERROR_9 --Otros
    END;
END;

/*
* Evalua si esta habilitado el registro de audotira segun el tipo de evento
*/
CREATE FUNCTION obtieneNivelLog(chrParamNivel CHARACTER, chrTipoEvento CHARACTER) RETURNS CHARACTER
    BEGIN 
	RETURN  
	 
	COALESCE(CASE chrTipoEvento
		WHEN C_EVENTO_10 THEN SUBSTRING(chrParamNivel FROM 1 FOR 1)
     	WHEN C_EVENTO_20 THEN SUBSTRING(chrParamNivel FROM 2 FOR 1)
		WHEN C_EVENTO_30 THEN SUBSTRING(chrParamNivel FROM 3 FOR 1)
		WHEN C_EVENTO_40 THEN SUBSTRING(chrParamNivel FROM 4 FOR 1)
		WHEN C_EVENTO_50 THEN SUBSTRING(chrParamNivel FROM 5 FOR 1)	
		WHEN C_EVENTO_60 THEN SUBSTRING(chrParamNivel FROM 6 FOR 1)
		WHEN C_EVENTO_70 THEN SUBSTRING(chrParamNivel FROM 7 FOR 1)
		WHEN C_EVENTO_80 THEN SUBSTRING(chrParamNivel FROM 8 FOR 1)		
		ELSE C_NO 
    END,C_FLAG_1);
END;

/*
* Completa una cadena con un caracter
*/
CREATE FUNCTION obtenerSecuencia(IN chrSec CHARACTER) RETURNS CHARACTER
    BEGIN 
	RETURN RIGHT('00' || chrSec,2);
END;

/*
 * Obtiene el codigo de la particion de la auditoria:  gruposervicio + anio + mes
*/
CREATE FUNCTION obtenerCodParticion(IN chrGrupoServicio CHARACTER)
RETURNS CHARACTER
BEGIN
	RETURN chrGrupoServicio || CAST(CURRENT_DATE AS CHAR FORMAT C_FORMATO_ANOMES_BD);				
END;

/*
 * Obtiene el dia de la fecha de proceso
*/
CREATE FUNCTION obtenerDia()
RETURNS INTEGER
BEGIN 
	RETURN EXTRACT(DAY FROM CURRENT_DATE);				
END;

/*
 * Valida el valor de los campos de auditoria
*/
CREATE FUNCTION validarCampoAuditoria (IN chrCampo CHARACTER)
RETURNS CHARACTER
BEGIN 
	IF (chrCampo IS NULL OR chrCampo = '') THEN
		RETURN '*';
	END IF;
	RETURN chrCampo; 
END;

/*
 * Funcion que alinea a la derecha o izquierda dependiendo de la longitud y el comodin que se envie
*/
CREATE FUNCTION FormatAlignCadena(IN cadena CHARACTER, IN longitud INT, IN char_repeat CHARACTER, IN align CHARACTER) RETURNS CHARACTER
BEGIN
	/*
	Parametros
	align --> Indica la alineacion: IZQ -> Izquierdo, DER -> derecha
	-- align: Para
	*/
	DECLARE int_length_orig INTEGER LENGTH(cadena);
	IF (int_length_orig > longitud) THEN
		SET cadena = LEFT(cadena, longitud);
	ELSEIF (int_length_orig < longitud) THEN
		IF (align = 'DER') THEN
			SET cadena = REPLICATE(char_repeat,(longitud-int_length_orig)) || cadena;
		ELSE
			SET cadena = cadena || REPLICATE(char_repeat,(longitud-int_length_orig));
		END IF;
	END IF;
	RETURN cadena;
END;

/*
 * Funcion que corrige la function LEFT cuando cadena es mayor a longitud 
*/
CREATE FUNCTION FormatAlignCadena_New(IN cadena CHARACTER, IN longitud INT, IN char_repeat CHARACTER, IN align CHARACTER, IN recort CHARACTER) RETURNS CHARACTER
BEGIN
	DECLARE int_length_orig INTEGER LENGTH(cadena);
	
	IF (int_length_orig > longitud) THEN
            IF (LENGTH(recort) > 0) THEN
                  IF (recort = 'DER') THEN
                        SET cadena = LEFT(cadena, longitud);
                  ELSE
                        SET cadena = RIGHT(cadena, longitud); 
                  END IF;
            ELSE
                  SET cadena = LEFT(cadena, longitud);
            END IF;
	ELSEIF (int_length_orig < longitud) THEN
		IF (align = 'DER') THEN
			SET cadena = REPLICATE(char_repeat,(longitud-int_length_orig)) || cadena;
		ELSE
			SET cadena = cadena || REPLICATE(char_repeat,(longitud-int_length_orig));
		END IF;
	END IF;
	RETURN cadena;
END;

/*
 * Controla el error para la llamada a un servicio proveedor CICS
*/ 
CREATE PROCEDURE obtenerCodMsjErrorCICS(IN refExceptionList REFERENCE, IN refEnvStatusRes REFERENCE) 
BEGIN
	
	CALL obtenerCodMsjError(refExceptionList,refEnvStatusRes);
	  
	
	IF CONTAINS(UPPER(refEnvStatusRes.status.busResponseMessage),'TIMEOUT') IS TRUE THEN
		SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_301();
		SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_301());
	ELSE
		SET refEnvStatusRes.detalleError = refEnvStatusRes.status.busResponseMessage;
		SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_302();
		SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_302());    			
		
	END IF; 
END;	

/*
 * Controla el error para la llamada a un servicio proveedor SOAP
*/ 
CREATE PROCEDURE obtenerCodMsjErrorREST(IN refExceptionList REFERENCE, IN refEnvStatusRes REFERENCE,IN refInputRoot REFERENCE) 
BEGIN
	CALL obtenerCodMsjError(refExceptionList,refEnvStatusRes);	
	
	IF (refInputRoot.HTTPResponseHeader."X-Original-HTTP-Status-Code" <> 200) THEN		
			
		SET refEnvStatusRes.status.responseType = getTIP_RSPTA_3();
		SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_302();
		SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_302());
		SET refEnvStatusRes.status.srvResponseCode = CAST(refInputRoot.HTTPResponseHeader."X-Original-HTTP-Status-Code" AS CHARACTER);
		SET refEnvStatusRes.status.srvResponseMessage = refInputRoot.HTTPResponseHeader."X-Original-HTTP-Status-Line";
		
		IF FIELDTYPE(refInputRoot.BLOB.BLOB) IS NOT NULL THEN
			SET refEnvStatusRes.detalleError = CAST(refInputRoot.BLOB.BLOB AS CHARACTER CCSID refInputRoot.Properties.CodedCharSetId);
			
			IF CONTAINS(UPPER(refInputRoot.HTTPResponseHeader."Content-Type"), 'JSON') THEN
				/*El backend envio un error de negocio/tecnico controlado en json*/
				SET refEnvStatusRes.status.responseType = getTIP_RSPTA_5();
				SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_502();
				SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_502());
				
				CREATE FIELD refEnvStatusRes.OutputRoot;
				DECLARE refOutput REFERENCE TO refEnvStatusRes.OutputRoot;
				CREATE LASTCHILD OF refOutput DOMAIN('JSON') PARSE (refInputRoot.BLOB.BLOB CCSID 819);
				SET refEnvStatusRes.JSON = refOutput.JSON;
				DELETE FIELD refEnvStatusRes.OutputRoot;
				
			ELSEIF UPPER(refInputRoot.HTTPResponseHeader."Content-Type") = 'TEXT' THEN
				SET refEnvStatusRes.status.responseType = getTIP_RSPTA_5();
				SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_502();
				SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_502());						
				SET refEnvStatusRes.status.srvResponseMessage = REPLACE(refEnvStatusRes.detalleError,'"','');
			END IF;					
		ELSE
			SET refEnvStatusRes.detalleError = CAST(refEnvStatusRes.status.srvResponseCode AS CHARACTER) || ' - ' || refEnvStatusRes.status.srvResponseMessage;
		END IF;
							
	ELSEIF CONTAINS(UPPER(refEnvStatusRes.status.busResponseMessage),'TIMEOUT') IS TRUE THEN
			SET refEnvStatusRes.status.responseType = getTIP_RSPTA_3();
			SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_301();
			SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_301());
			
	ELSEIF CONTAINS(UPPER(refEnvStatusRes.status.busResponseMessage),'SOCKET') IS TRUE THEN
			SET refEnvStatusRes.status.responseType = getTIP_RSPTA_3();
			SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_302();
			SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_302());				
    ELSE
    	SET refEnvStatusRes.status.responseType = getTIP_RSPTA_5();
		SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_502();
		SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_502());
	END IF;
END;

/*
 * Controla el error para la llamada a un servicio proveedor SOAP
*/ 
CREATE PROCEDURE obtenerCodMsjErrorSOAP(IN refExceptionList REFERENCE, IN refEnvStatusRes REFERENCE,IN refInputRoot REFERENCE) 
BEGIN
	CALL obtenerCodMsjError(refExceptionList,refEnvStatusRes);	
    
	IF (refInputRoot.HTTPResponseHeader."X-Original-HTTP-Status-Code" <> 200) THEN
		SET refEnvStatusRes.status.responseType = getTIP_RSPTA_3();
		SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_302();
		SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_302());
		SET refEnvStatusRes.status.srvResponseCode = refInputRoot.HTTPResponseHeader."X-Original-HTTP-Status-Code";
		SET refEnvStatusRes.status.srvResponseMessage = refInputRoot.HTTPResponseHeader."X-Original-HTTP-Status-Line";
		
		IF FIELDTYPE(refInputRoot.BLOB.BLOB) IS NOT NULL THEN 
			SET refEnvStatusRes.detalleError = CAST(refInputRoot.BLOB.BLOB AS CHARACTER CCSID refInputRoot.Properties.CodedCharSetId);
		ELSE
			SET refEnvStatusRes.detalleError = CAST(refEnvStatusRes.status.srvResponseCode AS CHARACTER) || ' - ' || refEnvStatusRes.status.srvResponseMessage;
		END IF;
		
		IF CONTAINS(UPPER(refInputRoot.HTTPResponseHeader."Content-Type"), 'XML') THEN /*Error no controlado por el Backend, devolvió un mensaje Fault*/
			SET refEnvStatusRes.status.responseType = getTIP_RSPTA_5();
			SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_502();
			SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_502());		
		END IF;
		
	ELSE
		IF CONTAINS(UPPER(refEnvStatusRes.status.busResponseMessage),'TIMEOUT') IS TRUE THEN
			SET refEnvStatusRes.status.responseType = getTIP_RSPTA_3();
			SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_301();
			SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_301());
			
		ELSEIF CONTAINS(UPPER(refEnvStatusRes.status.busResponseMessage),'SOCKET') IS TRUE THEN
			SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_302();
			SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_302());
			
		ELSE
			SET refEnvStatusRes.detalleError = refEnvStatusRes.status.busResponseMessage;
			SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_502();
			SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_502());    			
			
		END IF; 
		
	END IF;
END;

/*
 * Controla el error para la llamada a un servicio proveedor BD - JAQF
*/ 
CREATE PROCEDURE obtenerCodMsjErrorBD(IN refExceptionList REFERENCE, IN refEnvStatusRes REFERENCE,IN refEnvSQL REFERENCE,IN refCacheSrv REFERENCE,IN refCacheEqv REFERENCE) 
    BEGIN
    	
    	IF(LASTMOVE(refExceptionList)) THEN
    		CALL obtenerCodMsjError(refExceptionList,refEnvStatusRes);
    	END IF;
 
		SET refEnvStatusRes.status.busResponseCode = getCOD_ERROR_302();
		SET refEnvStatusRes.status.busResponseMessage = getMSJ_ERROR(getCOD_ERROR_302());    			
		
		IF (refEnvSQL.SQLCODE IS NOT NULL) THEN
			SET refEnvStatusRes.detalleError 				= refEnvSQL.SQLERRORTEXT;
			SET refEnvStatusRes.status.srvResponseCode 		= refEnvSQL.SQLNATIVEERROR;
			SET refEnvStatusRes.status.srvResponseMessage 	= refEnvSQL.SQLERRORTEXT;
			CALL obtenerEquivalenciaRespuestaBD(refEnvStatusRes,refCacheSrv,refCacheEqv,refEnvSQL.SQLCODE,refEnvSQL.SQLERRORTEXT,refEnvSQL.SQLNATIVEERROR,refEnvSQL.SQLSTATE);
		END IF;
    END;   

CREATE PROCEDURE GenerateOutput_WithOutInvalidCaracterUnicode(
	IN arg_output REFERENCE
)
	
BEGIN

	DECLARE blob_data_in BLOB;
	DECLARE int_CCSID INTEGER 1208; --UTF-8
	--DECLARE int_CCSID INTEGER 1200; --UTF-16
	DECLARE blob_data_out BLOB CAST('' AS BLOB CCSID int_CCSID);

	DECLARE options INTEGER BITOR(FolderBitStream, ValidateNone);
		
	SET blob_data_in = ASBITSTREAM(arg_output.XMLNSC OPTIONS options CCSID int_CCSID); --1208  (UTF-8)
	--SET blob_data_in = CAST(arg_output.XMLNSC AS BLOB CCSID int_CCSID); --1208  (UTF-8)
	
--	SET blob_data_out = stripNonValidXMLCharacters(blob_data_in);

	DECLARE i INTEGER 1;
	DECLARE int_longitud INTEGER LENGTH(blob_data_in);
	DECLARE blob_char BLOB;
	DECLARE int_char INTEGER;
		
	WHILE i <= int_longitud DO
		SET blob_char	= SUBSTRING(blob_data_in FROM i FOR 1);

		IF ((blob_char = X'0A') OR 
			(blob_char = X'0D') OR
			((blob_char >= X'20') AND (blob_char <= X'D7FF')) OR
			((blob_char >= X'E000') AND (blob_char<= X'FFFD')) OR
			((blob_char >= X'010000') AND (blob_char <= X'10FFFF'))
			) THEN
		--IF get_ValidaCharacterUnicode_XML(blob_character) THEN
			SET blob_data_out = blob_data_out || blob_char;
		END IF;
		SET i = i + 1;
	END WHILE;		

	SET arg_output.XMLNSC = NULL;
--	SET arg_output.XMLNSC = blob_data_out;

	CREATE LASTCHILD OF arg_output DOMAIN('XMLNSC') PARSE(blob_data_out CCSID int_CCSID);

	DECLARE ref_xmlnsc REFERENCE TO arg_output.XMLNSC.XMLNSC;
	IF LASTMOVE(ref_xmlnsc) THEN
		SET arg_output.XMLNSC			=	arg_output.XMLNSC.XMLNSC;
		SET arg_output.XMLNSC.XMLNSC	=	NULL;
	END IF;

END;

/*Funcion para listas cuyo tamaño depende de un campo que esta en la salida
longitudFija: Es la sumatoria de los tamanios de los campos que siempre vendran en la trama
longitudGrupo: Es la sumatoria de todos los tamanios de los campos que componen la lista  
*/
CREATE FUNCTION FormatDataWithGroup(IN cadena CHARACTER, IN longitudFija INT, IN longitudGrupo INT) RETURNS CHARACTER
BEGIN
	SET cadena = TRIM(TRAILING FROM cadena);
	
	DECLARE int_length_orig INTEGER LENGTH(cadena);
	
	IF LENGTH(cadena) < longitudFija THEN
		SET cadena = FormatAlignCadena(cadena, longitudFija, '0', 'IZQ');
	END IF;
	
	DECLARE int_mod INTEGER;

	SET int_mod = MOD((int_length_orig - longitudFija),longitudGrupo);
	IF (int_mod > 0) THEN
		DECLARE int_space INTEGER;
		SET int_space = longitudGrupo - int_mod;
		SET cadena = cadena || SPACE(int_space); 
	END IF;
	--SET cadena = get_Character_WithOut_NULL(cadena);
	RETURN cadena;
END;
