      ****************************************************************
700238* 703250 14/06/18 OLSA SERVICIOS CENTRALIZADOS DE TRANSAC BSE  *
      *==============================================================*
      * APLICATIVO      :  TLD                                       *
      * DESCRIPCION     :  AREA DE COMUNICACION - CAMPOS COMUNES     *
      *                 :  DE PROGRAMAS SUB-DESPACHO CONTACT CENTER  *
      * NOMBRE          :  BSECXCOM                                  *
      * LONGITUD        :  700 BYTES                                 *
      *==============================================================*
       01  BSECXCOM-COMMAREA.
           02  BSECXCOM-DATOS-INPUT.
      *            TIPO DE DOCUMENTO  DE IDENTIDAD
               03  BSECXCOM-CODOCIDE              PIC 9(02).
      *            NUMERO DE DOCUMENTO DE IDENTIDAD
               03  BSECXCOM-NUDOCIDE              PIC X(11).
      *            MONEDA DE LA TRANSACCION
               03  BSECXCOM-COMON                 PIC X(03).
      *            IMPORTE DE LA TRANSACCION
               03  BSECXCOM-IMTXN                 PIC 9(13)V9(2).
      *                  COMPRA Y VENTA CAMPOS DE I-O
      *            TIPO DE CAMBIO COMPRA
               03  BSECXCOM-TCAM-COMPRA           PIC 9(11)V9(6).
      *            TIPO DE CAMBIO VENTA
               03  BSECXCOM-TCAM-VENTA            PIC 9(11)V9(6).
      *        **************************************************
               03  BSECXCOM-FILLER-INPUT          PIC X(113).
      *-*
           02  BSECXCOM-COMMAREA-MPIS.
      *--------------------------------------------------------------*
      *            DATOS MEDIOS DE PAGO                              *
      *--------------------------------------------------------------*
               03  BSECXCOM-MEDIOS    OCCURS 2.
                   04  BSECXCOM-MEDIO-PAGO        PIC X(02).
                   04  BSECXCOM-TIPO-OPERACION    PIC X(01).
                   04  BSECXCOM-MONEDA-PAGO       PIC X(03).
                   04  BSECXCOM-IMPORTE-PAGO      PIC 9(13)V9(2).
                   04  BSECXCOM-MON-EQUIVALENTE   PIC X(03).
                   04  BSECXCOM-CAMBIO-ARBITRAJE  PIC 9(09)V9(6).       .
                   04  BSECXCOM-FLAG-TASA-NEG     PIC X(01).
                   04  BSECXCOM-IND-ITF           PIC X(01).
                   04  BSECXCOM-REDEFINICION      PIC X(70).
      *--------------------------------------------------------------*
      *        MEDIO DE PAGO - 00 - EFECTIVO                         *
      *--------------------------------------------------------------*
      *--------------------------------------------------------------*
      *        MEDIO DE PAGO - 04 - CUENTA CLIENTE                   *
      *--------------------------------------------------------------*
                   04  FILLER REDEFINES BSECXCOM-REDEFINICION.
      *                CODIGO DE CUENTA CONTROLES
                       06  BSECXCOM-CODIG-CTLES.
      *                        CODIGO DE CUENTA CONTROL 1 - BANCO
                           10  BSECXCOM-CO-CUENTA-CTL1       PIC X(02).
      *                        CODIGO DE CUENTA CONTROL 2 - MONEDA
                           10  BSECXCOM-CO-CUENTA-CTL2       PIC X(03).
      *                        CODIGO DE CUENTA CONTROL 4 - PRODUCTO
                           10  BSECXCOM-CO-CUENTA-CTL4       PIC X(03).
                       06  BSECXCOM-NUMERO-CTA-CLTE.
      *                        CODIGO DE CUENTA CONTROL 3 - TIENDA
                           10  BSECXCOM-CO-CUENTA-CTL3       PIC X(03).
      *                        NUMERO DE CUENTA
                           10  BSECXCOM-NRO-CUENTA           PIC X(10).
      *                    NUMERO DE ELECTRON UTILIZADA
                       06  BSECXCOM-NUMERO-ELECTRON          PIC X(16).
                       06  FILLER                            PIC X(19).
      *                         TIPO DE RETENCION 01-CHQMB  /02-CHQOB
      *                                          /03-CHQOBOP/04-CHQNEWY
                       06  BSECXCOM-TIPO-RETENCION           PIC 9(02).
      *                    GLOSA DINAMICA
                       06  BSECXCOM-GLOSA-DINAMICA           PIC X(12).
      *--------------------------------------------------------------*
      *            MEDIO DE PAGO - 05 - CUENTA CONTABLE              *
      *--------------------------------------------------------------*
                   04  FILLER REDEFINES BSECXCOM-REDEFINICION.
      *                    OFICINA EMISORA
                       06  BSECXCOM-OFI-EMISORA        PIC 9(03).
      *                    OFICINA RECEPTORA
                       06  BSECXCOM-OFI-RECEPT         PIC 9(03).
      *                    CUENTA CONTABLE
                       06  BSECXCOM-CTA-CTBLE          PIC 9(14).
      *                    NUMERO DE NOTA
                       06  BSECXCOM-NRO-NOTA           PIC 9(08).
      *                    INDICADOR DE NO AFECTO ITF
      *                             (S: NO AFECTO  N: SI AFECTO)
                       06  BSECXCOM-IND-NOAFECTO-CTBLE PIC X(01).
                       06  FILLER                      PIC X(41).
      *--------------------------------------------------------------*
      *        MEDIO DE PAGO - 06 - TARJETA DE CREDITO               *
      *                        61 - COMPRA TARJETA DE CREDITO        *
      *                        62 - DISPOSICION DE EFECTIVO          *
      *                        63 - PAGO DE TARJETA                  *
      *--------------------------------------------------------------*
                   04  FILLER REDEFINES BSECXCOM-REDEFINICION.
      *                CODIGO DE PRODUCTO
                       06  BSECXCOM-TARJETA-CREDITO        PIC  X(19).
                       06  BSECXCOM-MISMO-CLIENTE-TC       PIC  X(01).
                       06  BSECXCOM-GLOSA-DINAMICA-TC      PIC  X(25).
                       06  FILLER                          PIC  X(25).
      *-*
      *==CAMPOS COMUNES DE SALIDA
           02  BSECXCOM-DATOS-OUTPUT.
               03  BSECXCOM-COD-RET               PIC X(04).
               03  BSECXCOM-DES-RET               PIC X(88).
      *            APL CODIGO APLICATIVO ERROR
               03  BSECXCOM-COD-ERROR-APL         PIC X(03).
      *            TLD CODIGO DE ERROR DEL TOLD
               03  BSECXCOM-COD-ERROR-TOLD        PIC 9(04).
      *            TLD NUMERO DE OPERACION DEL TOLD
               03  BSECXCOM-NUM-OPER-TOLD         PIC 9(08).
      *            TLD FECHA DE OPERACION DEL TOLD
               03  BSECXCOM-FEC-OPER-TOLD         PIC 9(08).
      *        OTROS CAMPOS
      *            CODIGO DE AUTORIZACION
               03  BSECXCOM-CO-AUTORIZ            PIC 9(09).
      *            ESTADO DE LA CUENTA/TARJETA
               03  BSECXCOM-ST-ACCT               PIC X(02).
      *            CODIGO UNICO
               03  BSECXCOM-CO-UNICO              PIC X(10).
      *            PUNTO DE SERVICIO / USUARIO
               03  BSECXCOM-CO-PSE                PIC X(16).
      *            CODIGO TRANSACION TOLD
               03  BSECXCOM-TRX-TOLD-OUT          PIC X(08).
      *        SALDO DISPONIBLE Y CONTABLE
               03  BSECXCOM-IM-SALDO-DISP-S       PIC X(01).
               03  BSECXCOM-IM-SALDO-DISP         PIC 9(13)V9(02).
               03  BSECXCOM-IM-SALDO-CONT-S       PIC X(01).
               03  BSECXCOM-IM-SALDO-CONT         PIC 9(13)V9(02).
      *        ***************************************************
               03  BSECXCOM-FILLER-OUTPUT         PIC X(108).
      *--------------------------------------------------------
      *------------------      FIN BOOK      ------------------
      *--------------------------------------------------------
