************************************************************************
700281* 703543 18/07/17 BSEH - PPYSS DIGITALES - POST VENTA DIGITAL  ***
*     * 703543 11/07/17 OLSA CRIPTOGRAMA Y TOKEN                      **
***   * 104293 10/05/14 OLSA MULTIBUS EMPRESARIAL                     **
************************************************************************
      *--------------------------------------------------------------*
      *                                                              *
      *                    MULTI BUS EMPRESARIAL                     *
      *                                                              *
      *                                                              *
      *    AREAID               :  BSECCOMD                          *
      *                                                              *
      *    DESCRIPCION          :  COMMAREA BUS HOST CON BUS DISTRIB *
      *                                                              *
      *    LONGITUD DE REGISTRO :  9500                              *
      *                                                              *
      *--------------------------------------------------------------*
       01  BSECCOMD-REG.
      *        HEADER LONG=260
           03  BSECCOMD-HEADER-IN.
      *  CODIGO IDENTIFICACION DE LA TRAMA
               06  BSECCOMD-CO-TRAMA         PIC X(08).
      *  SUB CODIGO IDENTIFICACION DE LA TRAMA
               06  BSECCOMD-CO-SUB-TRAMA     PIC X(08).
      *  TIPO DE OPERACION
      *  'F' FINANCIERA
      *  'C' CONSULTA
               06  BSECCOMD-TI-OPER          PIC X(01).
      *  CONDICION DE LA OPERACION
      *  'N' NORMAL
      *  'E' EXTORNO
               06  BSECCOMD-IL-EXTO          PIC X(01).
      *  CODIGO DE RED
               06  BSECCOMD-CO-RED           PIC X(02).
      *  CODIGO DE SERVICIO
               06  BSECCOMD-CO-SRV           PIC X(03).
      *  CODIGO DE OFICINA
               06  BSECCOMD-CO-TIENDA        PIC 9(03).
      *  CODIGO DE USUARIO
               06  BSECCOMD-CO-USUARIO-IN    PIC X(08).
      *  CODIGO DE USUARIO AUTORIZADOR
               06  BSECCOMD-CO-USUARIO-AUT   PIC X(08).
      *  NOMBRE DE ESTACION
               06  BSECCOMD-DE-ESTACION      PIC X(30).
      *  FECHA DE ORIGEN
               06  BSECCOMD-FE-ORIGEN        PIC X(08).
      *  HORA  DE ORIGEN
               06  BSECCOMD-HO-ORIGEN        PIC X(06).
      *  NUMERO OPERACION SERVICIO ORIGEN
               06  BSECCOMD-NU-OPERAC-IN     PIC 9(08).
      *  NUMERO AUTORIZACION ORIGEN
               06  BSECCOMD-NU-AUTORIZ-IN    PIC 9(09).
      *  NUMERO A EXTORNAR
               06  BSECCOMD-NU-EXTORNO       PIC 9(08).
      *  LONGITUD DE LA TRAMA
               06  BSECCOMD-LN-SUB-TRAMA     PIC 9(04).
      *  TRAMAS ENCADENADAS A PROCESARSE
               06  BSECCOMD-EXEC-AREA-IN.
                 07  BSECCOMD-EXEC-AREA-IN-ENT OCCURS 8.
                   08  BSECCOMD-CO-SUB-TRAMA-IN  PIC X(08).
                   08  BSECCOMD-LN-SUB-TRAMA-IN  PIC 9(04).
      *  CODIGO LOG HOST (TIMESTAMP)
               06  BSECCOMD-CO-LOG           PIC X(26).
      *  CODIGO DE TRANSACCION CICS ORIGINAL
               06  BSECCOMD-CO-TRAN          PIC X(04).
      *  IDENTIFICADOR DE TRANSACCION EN REENTRANCIA
               06  BSECCOMD-IL-RECURSIVO     PIC X(01).
      *  INDETIFICADOR DE INSTANCIA I=INPUT, O=OUTPUT
               06  BSECCOMD-TI-INST          PIC X(01).
      *  DIRECCION DE AREA DE BSECCOMH
               06  BSECCOMD-DI-COMH          PIC X(4).
      *  LIBRE
               06  BSECCOMD-FILLER1          PIC X(209).
      *  AREA PARA SEGURIDAD
703964*    03  BSECCOMD-DE-SECVAL            PIC X(512).
703964     03  BSECCOMD-DE-SECVAL.
703964*  ESPACIO LIBRE
703964         06  BSECCOMD-DE-LIBRE         PIC X(19).
703964*  MONTO DE LA OPERACION
703964         06  BSECCOMD-IM-OPERACN       PIC 9(13)V99.
703964         06  BSECCOMD-IM-OPERACX       REDEFINES
703964             BSECCOMD-IM-OPERACN       PIC X(15).
703964*  MONEDA DE A OPERACION
703964         06  BSECCOMD-CO-MONEDA        PIC X(02).
703964*  TIPO TARJETA
703964         06  BSECCOMD-TI-TARJ          PIC X(07).
703964*  MARCA TARJETA
703964         06  BSECCOMD-DE-MARCA         PIC X(04).
703964*  CODIGO CLIENTE
703964         06  BSECCOMD-CO-CLIE          PIC X(10).
703964*  TOKEN BASADO EN TIEMPO
703964         06  BSECCOMD-DE-TOKEN         PIC X(32).
703964*  CRIPTOGRAMA
703964         06  BSECCOMD-DE-CRYPTO        PIC X(64).
703964*  NRO TARJETA
703964         06  BSECCOMD-NU-TARJETA       PIC X(19).
703964*  TRACK 2 DE LA TARJETA
703964         06  BSECCOMD-DE-TRACK2        PIC X(20).
703964*  PINBLOCK
703964         06  BSECCOMD-DE-PINBLOCK      PIC X(16).
703964*  AREA LIBRE
703964         06  BSECCOMD-FILLER11         PIC X(304).
      *        HEADER LONG=340
           03  BSECCOMD-HEADER-OUT.
      *  CODIGO DE USUARIO EJECUTANTE
               06  BSECCOMD-CO-USUARIO-OUT   PIC X(08).
      *  CODIGO DE RETORNO GENERAL
      *        06  BSECCOMD-CO-RETO          PIC 9(02).
               06  BSECCOMD-CO-RETO          PIC X(02).
      *  CODIGO DE RESPUESTA DEL BUS
      *        06  BSECCOMD-CO-RESP-BUS      PIC 9(04).
               06  BSECCOMD-CO-RESP-BUS      PIC X(04).
      *  CODIGO DE APLICACION DEL CODIGO DE RETORNO
               06  BSECCOMD-CO-APL-SRV       PIC X(03).
      *  CODIGO DE RESPUESTA DEL SERVICIO
      *        06  BSECCOMD-CO-RESP-SRV      PIC 9(04).
               06  BSECCOMD-CO-RESP-SRV      PIC X(04).
      *  DESCRIPCION DE RESPUESTA DEL BUS
               06  BSECCOMD-DE-RESP-BUS      PIC X(120).
      *  DESCRIPCION DE RESPUESTA DEL BUS
               06  BSECCOMD-DE-RESP-SRV      PIC X(120).
      *  NUMERO DE OPERACION
      *        06  BSECCOMD-NU-OPERAC-OUT    PIC 9(08).
               06  BSECCOMD-NU-OPERAC-OUT    PIC X(08).
      *  NUMERO DE AUTORIZADOR
      *        06  BSECCOMD-NU-AUTORIZ-OUT   PIC 9(09).
               06  BSECCOMD-NU-AUTORIZ-OUT   PIC X(09).
      *  PROGRAMA QUE RETORNO CON ERROR
               06  BSECCOMD-CO-PROG-RESP     PIC X(08).
      *  ULTIMA TRAMA PROCESADA
      *        06  BSECCOMD-NU-SUB-TRAMA     PIC 9(01).
               06  BSECCOMD-NU-SUB-TRAMA     PIC X(01).
      *  TRAMAS ENCADENADAS PROCESADAS
               06  BSECCOMD-EXEC-AREA-OUT.
                 07  BSECCOMD-EXEC-AREA-OUT-ENT OCCURS 8.
      *            08  BSECCOMD-LN-SUB-TRAMA-OUT PIC 9(04).
                   08  BSECCOMD-LN-SUB-TRAMA-OUT PIC X(04).
      *  CODIGO DE RETORNO DEL SERVICIO
      *        06  BSECCOMD-CO-RETO-SRV      PIC 9(02).
               06  BSECCOMD-CO-RETO-SRV      PIC X(02).
703964*  FLAG DESA
703964         06  BSECCOMD-IL-DES           PIC X(01).
      *  LIBRE
703964*        06  BSECCOMD-FILLER2          PIC X(19).
703964         06  BSECCOMD-FILLER2          PIC X(18).
      *  TRAMA DE INPUT/OUTPUT
           03  BSECCOMD-DATOS-IO             PIC X(8192).
      *-----------------------------------------------------------------
