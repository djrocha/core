BROKER SCHEMA ib.bus.core.mbh
PATH ib.bus.core.common;

DECLARE mbhns NAMESPACE 'http://interbank.com.pe/multibushost/dataobject/v1/';

CREATE COMPUTE MODULE SMF_BUS_SRV_MBH_PrepararMBH
	CREATE FUNCTION Main() RETURNS BOOLEAN
	BEGIN
		
		DECLARE REF_IN_MBH REFERENCE TO InputRoot.DataObject.mbhns:MBHRequest;
		DECLARE REF_IN_HEADER REFERENCE TO REF_IN_MBH.MBHHeader;
		DECLARE REF_OUT_CICS REFERENCE TO OutputRoot;
		DECLARE TRAMA_REQ CHARACTER '';
		
		-- Guardar Request original
		SET Environment.MBHRequest = REF_IN_MBH;
		
		-- Cambiar Message Definition
		CALL setearPropiedadesMensaje(OutputRoot,'','{}:BSECCOMDREG','',785,037);
		
		-- Crear Dominio de mensaje MRM para MBH
		CREATE LASTCHILD OF OutputRoot AS REF_OUT_CICS DOMAIN 'DFDL'; -- REF_OUT_CICS->DFDL
		CREATE FIELD OutputRoot.DFDL.BSECCOMDREG.BSECCOMD_HEADER_IN AS REF_OUT_CICS; -- REF_OUT_CICS->BSECCOMD_HEADER_IN
		
		-- Mapear campos cabecera entrada
		SET REF_OUT_CICS.BSECCOMD_CO_TRAMA 			= REF_IN_HEADER.codTrama;
		SET REF_OUT_CICS.BSECCOMD_CO_SUB_TRAMA 		= REF_IN_HEADER.codSubTrama;
		SET REF_OUT_CICS.BSECCOMD_TI_OPER 			= REF_IN_HEADER.tipoOperacion;
		SET REF_OUT_CICS.BSECCOMD_IL_EXTO 			= REF_IN_HEADER.esExtorno;
		SET REF_OUT_CICS.BSECCOMD_CO_RED 			= COALESCE(REF_IN_HEADER.codRed,'');
		SET REF_OUT_CICS.BSECCOMD_CO_SRV 			= REF_IN_HEADER.codServicio;
		SET REF_OUT_CICS.BSECCOMD_CO_TIENDA 		= COALESCE(REF_IN_HEADER.codTienda,'');
		SET REF_OUT_CICS.BSECCOMD_CO_USUARIO_IN 	= LEFT(UCASE(COALESCE(REF_IN_HEADER.codUsuario,'')),8);
		SET REF_OUT_CICS.BSECCOMD_CO_USUARIO_AUT	= COALESCE(REF_IN_HEADER.codAutorizador,'');
		SET REF_OUT_CICS.BSECCOMD_DE_ESTACION 		= COALESCE(NULLIF(TRIM(REF_IN_HEADER.nomEstacion),''), REF_IN_HEADER.codRed);
		SET REF_OUT_CICS.BSECCOMD_FE_ORIGEN 		= REF_IN_HEADER.fecOrigen;
		SET REF_OUT_CICS.BSECCOMD_HO_ORIGEN 		= REF_IN_HEADER.horOrigen;
		SET REF_OUT_CICS.BSECCOMD_NU_OPERAC_IN 		= COALESCE(REF_IN_HEADER.numOperacion,0);
		SET REF_OUT_CICS.BSECCOMD_NU_AUTORIZ_IN 	= COALESCE(REF_IN_HEADER.numAutorizacion,0);
		SET REF_OUT_CICS.BSECCOMD_NU_EXTORNO 		= COALESCE(REF_IN_HEADER.numExtorno,0);
		SET REF_OUT_CICS.BSECCOMD_LN_SUB_TRAMA 		= COALESCE(REF_IN_HEADER.longitudTrama,0);
		
		-- Agregar detalle de tramas
		CREATE LASTCHILD OF REF_OUT_CICS AS REF_OUT_CICS NAME 'BSECCOMD_EXEC_AREA_IN'; -- REF_OUT_CICS->BSECCOMD_EXEC_AREA_IN
		DECLARE CODTR CHARACTER '';
		DECLARE LENTR INTEGER 0;
		DECLARE N INTEGER 1;
		DECLARE REF_IN_TRAMAS REFERENCE TO REF_IN_MBH.MBHTramas;
		
		WHILE (N <= 8)
		DO
			SET CODTR = '';
			SET LENTR = 0;
			CREATE FIELD REF_OUT_CICS.BSECCOMD_EXEC_AREA_IN_ENT[N];
			IF (COALESCE(REF_IN_TRAMAS.Trama[N].codSubTrama,'') <>'') THEN
				-- Trama N existe
				SET CODTR = COALESCE(REF_IN_TRAMAS.Trama[N].codSubTrama, '');
				SET LENTR = COALESCE(REF_IN_TRAMAS.Trama[N].longSubTrama, 0);
				SET TRAMA_REQ = TRAMA_REQ || COALESCE(REF_IN_TRAMAS.Trama[N].dataSubTrama, '');
			END IF;
			SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_IN_ENT[N].BSECCOMD_CO_SUB_TRAMA_IN = CODTR;
			SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_IN_ENT[N].BSECCOMD_LN_SUB_TRAMA_IN = LENTR;
			
			SET N = N + 1;
		END WHILE;
		MOVE REF_OUT_CICS PARENT; -- REF_OUT_CICS->BSECCOMD_HEADER_IN
		
		-- adicional 1
		SET REF_OUT_CICS.BSECCOMD_CO_LOG = '';
		
		--Inicio Mapeo nuevos campos de MBH
		 SET REF_OUT_CICS.BSECCOMD_CO_TRAN = '';
		 SET REF_OUT_CICS.BSECCOMD_IL_RECURSIVO = '';
		 SET REF_OUT_CICS.BSECCOMD_TI_INST = '';
		 SET REF_OUT_CICS.BSECCOMD_DI_COMH = '';
		--Fin Mapeo nuevos campos de MBH
		
		SET REF_OUT_CICS.BSECCOMD_FILLER1 = '';
		
		-- adicional 2 
		
		CREATE FIELD OutputRoot.DFDL.BSECCOMDREG.BSECCOMD_DE_SECVAL;
		MOVE REF_OUT_CICS TO OutputRoot.DFDL.BSECCOMDREG.BSECCOMD_DE_SECVAL;
		
		--Inicio Mapeo nuevos campos de MBH
		SET REF_OUT_CICS.BSECCOMD_DE_LIBRE = '';
		SET REF_OUT_CICS.BSECCOMD_IM_OPERACX = COALESCE(REF_IN_HEADER.seguridad.montoOperacion,'');
		SET REF_OUT_CICS.BSECCOMD_CO_MONEDA = COALESCE(REF_IN_HEADER.seguridad.monedaOperacion,'');
		SET REF_OUT_CICS.BSECCOMD_TI_TARJ = COALESCE(REF_IN_HEADER.seguridad.tipoTarjeta,'');
		SET REF_OUT_CICS.BSECCOMD_DE_MARCA = COALESCE(REF_IN_HEADER.seguridad.marcaTarjeta,'');
		SET REF_OUT_CICS.BSECCOMD_CO_CLIE = COALESCE(REF_IN_HEADER.seguridad.codigoCliente,'');
		SET REF_OUT_CICS.BSECCOMD_DE_TOKEN = COALESCE(REF_IN_HEADER.seguridad.token,'');
		SET REF_OUT_CICS.BSECCOMD_DE_CRYPTO = COALESCE(REF_IN_HEADER.seguridad.criptograma,'');
		SET REF_OUT_CICS.BSECCOMD_NU_TARJETA = COALESCE(REF_IN_HEADER.seguridad.numeroTarjeta,'');
		SET REF_OUT_CICS.BSECCOMD_DE_TRACK2 = COALESCE(REF_IN_HEADER.seguridad.track2,'');
		SET REF_OUT_CICS.BSECCOMD_DE_PINBLOCK = COALESCE(REF_IN_HEADER.seguridad.pinBlock,'');
		SET REF_OUT_CICS.BSECCOMD_FILLER11 = '';
		--Fin Mapeo nuevos campos de MBH
		
		MOVE REF_OUT_CICS PARENT; -- REF_OUT_CICS->BSECCOMDREG
		
		-- Completar cabecera salida vacia 
		CREATE FIELD REF_OUT_CICS.BSECCOMD_HEADER_OUT AS REF_OUT_CICS; -- REF_OUT_CICS->BSECCOMD_HEADER_OUT
		SET REF_OUT_CICS.BSECCOMD_CO_USUARIO_OUT = '';
		SET REF_OUT_CICS.BSECCOMD_CO_RETO = '00';
		SET REF_OUT_CICS.BSECCOMD_CO_RESP_BUS = '0000';
		SET REF_OUT_CICS.BSECCOMD_CO_APL_SRV = '';
		SET REF_OUT_CICS.BSECCOMD_CO_RESP_SRV = '0000';
		SET REF_OUT_CICS.BSECCOMD_DE_RESP_BUS = '';
		SET REF_OUT_CICS.BSECCOMD_DE_RESP_SRV = '';
		SET REF_OUT_CICS.BSECCOMD_NU_OPERAC_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_NU_AUTORIZ_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_CO_PROG_RESP = 0;
		SET REF_OUT_CICS.BSECCOMD_NU_SUB_TRAMA = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[1].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[2].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[3].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[4].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[5].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[6].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[7].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_EXEC_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[8].BSECCOMD_LN_SUB_TRAMA_OUT = 0;
		SET REF_OUT_CICS.BSECCOMD_CO_RETO_SRV = '';
		
		--Inicio Nuevo Campo de MBH
		SET REF_OUT_CICS.BSECCOMD_IL_DES = '';
		--Fin Nuevo Campo de MBH
		
		SET REF_OUT_CICS.BSECCOMD_FILLER2 = '';
		MOVE REF_OUT_CICS PARENT; -- REF_OUT_CICS->DFDL
		
		-- Agregar Tramas de Request
		CREATE FIELD REF_OUT_CICS.BSECCOMD_DATOS_IO VALUE TRAMA_REQ;
		
		DECLARE txt_mirrorTran CHARACTER;
		DECLARE boo_eibtrnidOnly BOOLEAN;
		
		DECLARE REF_SRV_CICS REFERENCE TO REF_IN_HEADER.cics;
		
		IF (LASTMOVE(REF_SRV_CICS)) THEN
			SET txt_mirrorTran		= REF_SRV_CICS.mirrorTran;
			IF (REF_SRV_CICS.eibtrnidOnly = 1) THEN
				SET boo_eibtrnidOnly	= TRUE;
			ELSE
				SET boo_eibtrnidOnly	= FALSE;
			END IF;
		END IF;
		
		SET Environment.Data.errorCICS = FALSE;
		SET OutputLocalEnvironment.Destination.CICS.mirrorTran		= txt_mirrorTran; 
		SET OutputLocalEnvironment.Destination.CICS.eibtrnidOnly	= boo_eibtrnidOnly; 
		
		SET OutputLocalEnvironment.Destination.CICS.requestTimeoutSecs	= CAST(getParmSecSrv(Environment,'TIMEOUT') AS INTEGER);
		
		SET Environment.CICS.tipo = getParmSecSrv(Environment,'TIPOCICS');
		
		--Preparacion del monitoreo del servicio - Hora de inicio
		CALL prepararEntornoInicialMonitoreo(Environment);
		RETURN TRUE;
	END;
END MODULE;

CREATE COMPUTE MODULE SMF_BUS_SRV_MBH_PrepararMBHResp
	CREATE FUNCTION Main() RETURNS BOOLEAN
	BEGIN
		
		--Preparacion del monitoreo del servicio - Tiempo de respuesta
		CALL prepararEntornoFinalMonitoreo(Environment);
		
		IF(Environment.Data.errorCICS = FALSE) THEN
			DECLARE REF_IN_CICS REFERENCE TO InputRoot.DFDL.BSECCOMDREG;
			CREATE FIELD Environment.InputRoot;
			CREATE LASTCHILD OF Environment.InputRoot DOMAIN('DFDL');
			SET Environment.InputRoot.DFDL = InputRoot.DFDL;
			
			DECLARE chrcodError INTEGER CAST(REF_IN_CICS.BSECCOMD_HEADER_OUT.BSECCOMD_CO_RETO AS INTEGER DEFAULT -1);
			SET Environment.InputRoot.DFDL.BSECCOMDREG.BSECCOMD_HEADER_IN.BSECCOMD_FILLER1='';
			
			DECLARE blbMsg BLOB ASBITSTREAM(Environment.InputRoot.DFDL OPTIONS EmbeddedBitStream);
			DELETE FIELD Environment.InputRoot;
			
			-- Crear Estructura de respuesta
			CREATE FIELD Environment.MBHResponse.MBHHeader; -- REF_OUT_MBH->MBHResponse->MBHHeader
			DECLARE refEnvHead REFERENCE TO Environment.MBHResponse.MBHHeader;
			DECLARE refCicsHead REFERENCE TO REF_IN_CICS.BSECCOMD_HEADER_OUT;
			/*limpieza del valor del campo respuesta*/
			CALL GenerateClearMessageResponse(refCicsHead,Environment);
			-- Mapear Campos de cabecera
			SET refEnvHead.codUsuario = refCicsHead.BSECCOMD_CO_USUARIO_OUT;
			SET refEnvHead.codRetorno = refCicsHead.BSECCOMD_CO_RETO;
			SET refEnvHead.aplCodRetorno = refCicsHead.BSECCOMD_CO_APL_SRV;
			SET refEnvHead.codRespBus = refCicsHead.BSECCOMD_CO_RESP_BUS;
			SET refEnvHead.descRespBus = refCicsHead.BSECCOMD_DE_RESP_BUS;
			SET refEnvHead.codRespServicio = refCicsHead.BSECCOMD_CO_RESP_SRV;
			SET refEnvHead.descRespServicio = refCicsHead.BSECCOMD_DE_RESP_SRV;
			SET refEnvHead.numOperacion = refCicsHead.BSECCOMD_NU_OPERAC_OUT;
			SET refEnvHead.numAutorizador = refCicsHead.BSECCOMD_NU_AUTORIZ_OUT;
			SET refEnvHead.progRetError = refCicsHead.BSECCOMD_CO_PROG_RESP;
			SET refEnvHead.ultTramaProc = refCicsHead.BSECCOMD_NU_SUB_TRAMA;
			SET refEnvHead.codRetServicio = refCicsHead.BSECCOMD_CO_RETO_SRV;
			
			/*Se crea Environment.StatusRes con el codigo y mensaje de CICS*/
			CALL crearResponseStatus(Environment,NULL,NULL,NULL,refCicsHead.BSECCOMD_CO_RESP_SRV,refCicsHead.BSECCOMD_DE_RESP_SRV);
			/*Se evalua si el codigo de respuesta de CICS debe ser manejado como un caso de EXITO*/			
			CALL obtenerEquivalenciaRespuestaCICS_CategoriaExito(Environment.StatusRes,Environment,Environment.Equivalencias);
			DECLARE responseType INTEGER COALESCE(Environment.StatusRes.status.responseType, -1);
			DELETE FIELD Environment.StatusRes.status;
						
			/*Si MBH devuelve exito ("0") o si devuelve algun codigo mapeado en equivalencia como parte de la categoria de exito*/			
			IF(chrcodError = 0 OR responseType = 0) THEN

				IF(chrcodError = 0) THEN
					-- Mapear Tramas de respuesta
					CREATE FIELD Environment.MBHResponse.MBHTramas;
					DECLARE refEnvTramas REFERENCE TO Environment.MBHResponse.MBHTramas;
					DECLARE REF_AREA_OUT REFERENCE TO refCicsHead.BSECCOMD_EXEC_AREA_OUT;
					
					DECLARE N INTEGER 1;
					DECLARE PTRTRM INTEGER 1;
					DECLARE LNTRM INTEGER 0;
					WHILE (N <= 8)
					DO
						SET LNTRM = REF_AREA_OUT.BSECCOMD_EXEC_AREA_OUT_ENT[N].BSECCOMD_LN_SUB_TRAMA_OUT;
						IF LNTRM > 0 THEN
							-- Trama N existe
							CREATE LASTCHILD OF refEnvTramas NAME 'Trama';
							SET refEnvTramas.Trama[N].codSubTrama = Environment.MBHRequest.MBHTramas.Trama[N].codSubTrama;
							
							DECLARE refTrama REFERENCE TO Environment.MBHRequest.MBHTramas;
							DECLARE chr_tramaOut CHARACTER '';
							DECLARE flgListDependingOn BOOLEAN COALESCE(Environment.CICS.IsListDependingOn, FALSE);
							 
							IF flgListDependingOn IS FALSE THEN
								SET chr_tramaOut = FormatAlignCadena(RTRIM(SUBSTRING(REF_IN_CICS.BSECCOMD_DATOS_IO FROM PTRTRM FOR LNTRM)),CAST(refTrama.Trama[N].longSubTrama AS INTEGER),' ','IZQ');
							ELSE							
								SET chr_tramaOut = SUBSTRING(REF_IN_CICS.BSECCOMD_DATOS_IO FROM PTRTRM FOR LNTRM);
								/*Para caso cuando la copy tenga lista cuyas ocurrencias que dependa de un campo, y se autcompleta la trama para casos cuando lista no viene completa*/
								--SET chr_tramaOut = FormatDataWithGroup(RTRIM(SUBSTRING(REF_IN_CICS.BSECCOMD_DATOS_IO FROM PTRTRM FOR LNTRM)), Environment.CICS.LongitudFija, Environment.CICS.LongitudGrupo);								
							END IF;
							
							SET refEnvTramas.Trama[N].dataSubTrama = chr_tramaOut;--OutputRoot.{refTrama.Trama[N].msetTypeSubTrama};	
							SET refEnvTramas.Trama[N].longSubTrama = Environment.MBHRequest.MBHTramas.Trama[N].longSubTrama;
							-- A esta variable se le esta sumando 1 para que cuando reciba la trama lo parsee desde el espacio 1					
							--ANLT: La variable PTRTRM es un acumulador, acumula la posicion inicial con la cual realizara el substring a la trama out BSECCOMD_DATOS_IO en cada iteracion.
							SET PTRTRM = LNTRM+PTRTRM;
							-- Se esta poniendo Null al OutputRoot para que cuando busque por el codigo de la trama solo busque a la unica trama que se le agrega
							SET OutputRoot = NULL;
						END IF;
						
						SET N = N + 1;
					END WHILE;
				END IF;
				
				CALL crearResponseStatus(Environment,getTIP_RSPTA_0(),0,getMSJ_EXITO_ESB(),refCicsHead.BSECCOMD_CO_RETO,refCicsHead.BSECCOMD_DE_RESP_SRV);
				SET Environment.StatusRes.exception = FALSE;
				
			ELSE
			
				DECLARE chrCodRes CHARACTER FormatAlignCadena(refCicsHead.BSECCOMD_CO_RESP_SRV,4,'0','IZQ');
				CALL crearResponseStatus(Environment,getTIP_RSPTA_5(),getCOD_ERROR_502(),getMSJ_ERROR(getCOD_ERROR_502()),refCicsHead.BSECCOMD_CO_RESP_SRV,refCicsHead.BSECCOMD_DE_RESP_SRV);
		
				IF (chrCodRes = '0000') THEN
					CALL crearResponseStatus(Environment,getTIP_RSPTA_5(),getCOD_ERROR_502(),getMSJ_ERROR(getCOD_ERROR_502()),refCicsHead.BSECCOMD_CO_RESP_BUS,refCicsHead.BSECCOMD_DE_RESP_BUS);
				END IF;
				
				SET Environment.StatusRes.detalleError=CAST(blbMsg AS CHARACTER CCSID InputProperties.CodedCharSetId);
				SET Environment.StatusRes.exception = TRUE;
			END IF;
			
		ELSE
			CALL crearResponseStatus(Environment,getTIP_RSPTA_3(),getCOD_ERROR_302(),getMSJ_ERROR(getCOD_ERROR_302()),InputLocalEnvironment.CICS.AbendCode,InputLocalEnvironment.CICS.AbendCode);
			SET Environment.StatusRes.detalleError = InputLocalEnvironment.CICS.AbendCode;
			SET Environment.StatusRes.exception = TRUE;
		END IF;
		
		IF(Environment.StatusRes.exception IS TRUE) THEN
			CALL obtenerEquivalenciaRespuestaCICS(Environment.StatusRes,Environment,Environment.Equivalencias,InputLocalEnvironment.CICS);
		END IF;
		
		SET Environment.Srv.Secuencia = NULL;
		
		PROPAGATE TO TERMINAL 0;
		
		-- Borrar Request original
		SET Environment.MBHRequest = NULL;
		RETURN FALSE;
	END;
END MODULE;

CREATE COMPUTE MODULE SMF_BUS_SRV_MBH_PrepararMBHError
	CREATE FUNCTION Main() RETURNS BOOLEAN
	BEGIN
		--Preparacion del monitoreo del servicio - Tiempo de respuesta
		CALL prepararEntornoFinalMonitoreo(Environment);
		SET Environment.Data.errorCICS = TRUE;
		SET OutputRoot = InputRoot;
		
		SET Environment.Srv.Secuencia = NULL;
		
		IF (InputLocalEnvironment.CICS.AbendCode IS NOT NULL)THEN
			PROPAGATE TO TERMINAL 1;
		ELSE
			PROPAGATE TO TERMINAL 2;
		END IF;
		
		RETURN FALSE;
	END;
END MODULE;
CREATE PROCEDURE GenerateClearMessageResponse(IN arg_output REFERENCE, IN refEnv REFERENCE)
BEGIN
	DECLARE blob_data_in BLOB;
	DECLARE int_CCSID INTEGER 1208; --UTF-8
	--DECLARE int_CCSID INTEGER 1200; --UTF-16
	DECLARE blob_data_out BLOB CAST('' AS BLOB CCSID int_CCSID);
	SET blob_data_in = CAST(arg_output.BSECCOMD_DE_RESP_SRV AS BLOB CCSID int_CCSID); --1208  (UTF-8)
	DECLARE i INTEGER 1;
	DECLARE int_longitud INTEGER LENGTH(blob_data_in);
	DECLARE blob_char BLOB;
	DECLARE int_char INTEGER;
	WHILE i <= int_longitud DO
		SET blob_char	= SUBSTRING(blob_data_in FROM i FOR 1);
		IF (blob_char = X'00') THEN
			SET blob_data_out = blob_data_out || X'20';
		ELSE
			SET blob_data_out = blob_data_out || blob_char;
		END IF;
		SET i = i + 1;
	END WHILE;
	SET arg_output.BSECCOMD_DE_RESP_SRV = CAST(blob_data_out AS CHARACTER CCSID 1208);
END;